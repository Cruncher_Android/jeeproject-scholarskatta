import { DatabaseServiceService } from "./services/database-service.service";
import { Storage } from "@ionic/storage";
import { Component } from "@angular/core";
import { Network } from "@ionic-native/network/ngx";
import {
  Platform,
  PopoverController,
  NavController,
  AlertController,
} from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Router } from "@angular/router";
import { StorageServiceService } from "./services/storage-service.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  output: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private popoverController: PopoverController,
    private navController: NavController,
    private alertController: AlertController,
    private network: Network,
    private databaseServiceService: DatabaseServiceService,
    private storageServiceService: StorageServiceService
  ) {
    this.initializeApp();

    this.platform.backButton.subscribeWithPriority(0, () => {
      console.log("in app back");
      if (this.router.url === "/dashboard/home") {
        this.createAlert();
      } else if (this.router.url === "/dashboard/unit-test") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (this.router.url.startsWith("/main")) {
        storage.get("userDetailsPopover").then((data) => {
          if (data == true) {
          } else this.createAlert();
        });
      } else if (this.router.url === "/dashboard/question-paper") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (this.router.url.startsWith("/chapter-list")) {
        this.storage.ready().then((ready) => {
          if (ready) {
            this.storage.get("testTypeSelectionPopover").then((data) => {
              this.storage.get("changeSettingsPopOver").then((result) => {
                if (result == true) {
                } else if (data == true) {
                  this.popoverController.dismiss();
                } else {
                  this.navController.pop();
                }
              });
            });
          }
        });
      } else if (this.router.url == "/dashboard/bookmark") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (this.router.url == "/dashboard/view-test") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (this.router.url == "/dashboard/about-us") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (router.url == "/dashboard/notes") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (router.url.startsWith("/notes-chapters")) {
        this.router.navigateByUrl("/dashboard/notes");
      } else if (this.router.url == "/dashboard/performance-graph") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (this.router.url == "/dashboard/settings") {
        this.router.navigateByUrl("/dashboard/home");
      } else if (this.router.url == "/offline-verification") {
        return;
      } else {
        this.navController.back();
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.databaseServiceService.createDataBase();
      console.log("entry url", this.router.url);
      this.storage.get("userLoggedIn").then((data) => {
        this.storageServiceService.userLoggedIn = data;
        if (data == true || data == "demo") {
          this.router.navigateByUrl("/dashboard/home");
        } else {
          this.router.navigateByUrl("/main");
        }
        this.statusBar.show();
        this.splashScreen.hide();
      });
    });
  }

  async createAlert() {
    const alert = await this.alertController.create({
      header: "Close App",
      cssClass: "alert-title",
      subHeader: "Do you want to close app ?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
        },
        {
          text: "Close App",
          handler: () => {
            navigator["app"].exitApp();
          },
        },
      ],
    });
    await alert.present();
  }
}
