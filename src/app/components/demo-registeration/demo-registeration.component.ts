import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { UserInfoService } from "./../../services/user-info.service";
import { Storage } from "@ionic/storage";
import { PopoverController, LoadingController, ActionSheetController } from "@ionic/angular";
import { LoadingServiceService } from "./../../services/loading-service.service";
import { HttpClient } from "@angular/common/http";
import { ApiServiceService } from "./../../services/api-service.service";
import { OtpServiceService } from "./../../services/otp-service.service";
import { StorageServiceService } from "./../../services/storage-service.service";
import { Crop } from "@ionic-native/crop/ngx";
import { File } from "@ionic-native/file/ngx";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Action } from 'rxjs/internal/scheduler/Action';

@Component({
  selector: "app-demo-registeration",
  templateUrl: "./demo-registeration.component.html",
  styleUrls: ["./demo-registeration.component.scss"],
})
export class DemoRegisterationComponent implements OnInit {
  demoInfo: {
    firstName: string;
    lastName: string;
    contactNo: string;
    email: string;
    distributerId: string;
    appId: string;
    date: string;
    area: string;
    imageData: any;
  };
  date: any;
  month: any;
  year: any;
  fullYear: any;
  randomNumber: number;

  enteredOTP: string = "";

  otpMatch: boolean = false;

  otpSent: boolean = false;

  imageURl: string = '';
  constructor(
    private router: Router,
    private storage: Storage,
    private popoverController: PopoverController,
    private loadingController: LoadingController,
    private actionSheetController: ActionSheetController,
    private http: HttpClient,
    private databaseServiceService: DatabaseServiceService,
    private userInfoService: UserInfoService,
    private loadingServiceService: LoadingServiceService,
    private apiService: ApiServiceService,
    private otpService: OtpServiceService,
    private storageService: StorageServiceService,
    private crop: Crop,
    private file: File,
    private camera: Camera,
  ) {}

  ngOnInit() {
    this.date = new Date().getDate();
    this.month = new Date().getMonth() + 1;
    this.fullYear = new Date().getFullYear();
    this.demoInfo = {
      contactNo: "",
      email: "",
      firstName: "",
      lastName: "",
      distributerId: "",
      appId: "",
      date: "",
      area: "",
      imageData: "",
    };
  }

  onSubmit() {
    this.demoInfo.date =
      this.date.toString() +
      "-" +
      this.month.toString() +
      "-" +
      this.fullYear.toString();
    this.demoInfo.distributerId = "01";
    this.demoInfo.appId = "2";
    this.loadingServiceService.createLoading("Please wait").then((_) => {
      this.http
        .post(`${this.apiService.apiUrl}setDemo`, this.demoInfo)
        .subscribe((data) => {
          if (data) {
            this.databaseServiceService
              .getDatabaseState()
              .subscribe((ready) => {
                if (ready) {
                  this.userInfoService
                    .insertIntoTableForDemo(this.demoInfo)
                    .then((_) => {
                      this.storage.ready().then((ready) => {
                        if (ready) {
                          this.storage.get("demoRegistered").then((data) => {
                            if (data == true) {
                              this.loadingController.dismiss().then((_) => {
                                this.router.navigateByUrl("/dashboard/home");
                                this.popoverController.dismiss();
                                this.storage.set("userLoggedIn", "demo");
                                this.storageService.userLoggedIn = "demo";
                                this.demoInfo = {
                                  contactNo: "",
                                  email: "",
                                  firstName: "",
                                  lastName: "",
                                  distributerId: "",
                                  appId: "",
                                  date: "",
                                  area: "",
                                  imageData: "",
                                };
                              });
                            } else {
                              let date = new Date().getDate().toString();
                              let month = (
                                new Date().getMonth() + 2
                              ).toString();
                              let year = new Date()
                                .getFullYear()
                                .toString()
                                .substr(2);
                              if (date.length == 1) date = "0" + date;
                              if (month.length == 1) month = "0" + month;
                              this.storage
                                .set("demoRegistered", true)
                                .then((_) => {
                                  this.storage.set(
                                    "expireDate",
                                    year + month + date
                                  );
                                  this.loadingController.dismiss().then((_) => {
                                    this.router.navigateByUrl(
                                      "/dashboard/home"
                                    );
                                    this.popoverController.dismiss();
                                    this.storage.set("userLoggedIn", "demo");
                                    this.storageService.userLoggedIn = "demo";
                                  });
                                });
                            }
                          });
                        }
                      });
                    });
                }
              });
          }
        });
    });
  }

  sendOtp() {
    this.otpSent = true;
    this.otpMatch = false;
    this.enteredOTP = "";
    this.randomNumber = Math.floor(1000 + Math.random() * 9000);
    console.log(this.randomNumber);
    // console.log(this.number);
    // console.log(this.storageService.userInfo.StudentContact);
    this.otpService.sendOtp(this.demoInfo.contactNo, this.randomNumber);
  }

  verifyOtp(event) {
    let otp = event.target.value;
    console.log(this.randomNumber, otp);
    if (otp.length == 4) {
      if (Number.parseInt(otp) == this.randomNumber) {
        console.log("in if verify otp");
        this.otpMatch = true;
      } else this.otpMatch = false;
    } else this.otpMatch = false;
  }

  async onProfile() {
    const action = await this.actionSheetController.create({
      buttons: [
        {
          text: "Choose from gallary",
          handler: () => {
            this.chooseFile();
          },
        },
        {
          text: "Open Camera",
          handler: () => {
            this.openCamera();
          },
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
    });
    await action.present();
  }

  chooseFile() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      correctOrientation: true,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData);
    });
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData);
    });
  }

  cropImage(imageData) {
    this.crop.crop(imageData, { quality: 100 }).then((cropedImage) => {
      console.log("cropped image", cropedImage);
      let imagePath = cropedImage.split("?")[0];
      let copyPath = imagePath;
      let splitPath = copyPath.split("/");
      let imageName = splitPath[splitPath.length - 1];
      let filePath = imagePath.split(imageName)[0];
      console.log("image name", imageName);
      console.log("file path", filePath);

      this.file.readAsDataURL(filePath, imageName).then((dataUrl) => {
        console.log("data url", dataUrl);
        this.imageURl = dataUrl;
        this.demoInfo.imageData = dataUrl;
      });
    });
  }
}
