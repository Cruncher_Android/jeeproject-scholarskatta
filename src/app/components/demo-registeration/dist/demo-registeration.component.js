"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.DemoRegisterationComponent = void 0;
var core_1 = require("@angular/core");
var DemoRegisterationComponent = /** @class */ (function () {
    function DemoRegisterationComponent(router, storage, popoverController, loadingController, actionSheetController, http, databaseServiceService, userInfoService, loadingServiceService, apiService, otpService, storageService, crop, file, camera) {
        this.router = router;
        this.storage = storage;
        this.popoverController = popoverController;
        this.loadingController = loadingController;
        this.actionSheetController = actionSheetController;
        this.http = http;
        this.databaseServiceService = databaseServiceService;
        this.userInfoService = userInfoService;
        this.loadingServiceService = loadingServiceService;
        this.apiService = apiService;
        this.otpService = otpService;
        this.storageService = storageService;
        this.crop = crop;
        this.file = file;
        this.camera = camera;
        this.enteredOTP = "";
        this.otpMatch = false;
        this.otpSent = false;
        this.imageURl = '';
    }
    DemoRegisterationComponent.prototype.ngOnInit = function () {
        this.date = new Date().getDate();
        this.month = new Date().getMonth() + 1;
        this.fullYear = new Date().getFullYear();
        this.demoInfo = {
            contactNo: "",
            email: "",
            firstName: "",
            lastName: "",
            distributerId: "",
            appId: "",
            date: "",
            area: "",
            imageData: ""
        };
    };
    DemoRegisterationComponent.prototype.onSubmit = function () {
        var _this = this;
        this.demoInfo.date =
            this.date.toString() +
                "-" +
                this.month.toString() +
                "-" +
                this.fullYear.toString();
        this.demoInfo.distributerId = "01";
        this.demoInfo.appId = "2";
        this.loadingServiceService.createLoading("Please wait").then(function (_) {
            _this.http
                .post(_this.apiService.apiUrl + "setDemo", _this.demoInfo)
                .subscribe(function (data) {
                if (data) {
                    _this.databaseServiceService
                        .getDatabaseState()
                        .subscribe(function (ready) {
                        if (ready) {
                            _this.userInfoService
                                .insertIntoTableForDemo(_this.demoInfo)
                                .then(function (_) {
                                _this.storage.ready().then(function (ready) {
                                    if (ready) {
                                        _this.storage.get("demoRegistered").then(function (data) {
                                            if (data == true) {
                                                _this.loadingController.dismiss().then(function (_) {
                                                    _this.router.navigateByUrl("/dashboard/home");
                                                    _this.popoverController.dismiss();
                                                    _this.storage.set("userLoggedIn", "demo");
                                                    _this.storageService.userLoggedIn = "demo";
                                                    _this.demoInfo = {
                                                        contactNo: "",
                                                        email: "",
                                                        firstName: "",
                                                        lastName: "",
                                                        distributerId: "",
                                                        appId: "",
                                                        date: "",
                                                        area: "",
                                                        imageData: ""
                                                    };
                                                });
                                            }
                                            else {
                                                var date_1 = new Date().getDate().toString();
                                                var month_1 = (new Date().getMonth() + 2).toString();
                                                var year_1 = new Date()
                                                    .getFullYear()
                                                    .toString()
                                                    .substr(2);
                                                if (date_1.length == 1)
                                                    date_1 = "0" + date_1;
                                                if (month_1.length == 1)
                                                    month_1 = "0" + month_1;
                                                _this.storage
                                                    .set("demoRegistered", true)
                                                    .then(function (_) {
                                                    _this.storage.set("expireDate", year_1 + month_1 + date_1);
                                                    _this.loadingController.dismiss().then(function (_) {
                                                        _this.router.navigateByUrl("/dashboard/home");
                                                        _this.popoverController.dismiss();
                                                        _this.storage.set("userLoggedIn", "demo");
                                                        _this.storageService.userLoggedIn = "demo";
                                                    });
                                                });
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
                }
            });
        });
    };
    DemoRegisterationComponent.prototype.sendOtp = function () {
        this.otpSent = true;
        this.otpMatch = false;
        this.enteredOTP = "";
        this.randomNumber = Math.floor(1000 + Math.random() * 9000);
        console.log(this.randomNumber);
        // console.log(this.number);
        // console.log(this.storageService.userInfo.StudentContact);
        this.otpService.sendOtp(this.demoInfo.contactNo, this.randomNumber);
    };
    DemoRegisterationComponent.prototype.verifyOtp = function (event) {
        var otp = event.target.value;
        console.log(this.randomNumber, otp);
        if (otp.length == 4) {
            if (Number.parseInt(otp) == this.randomNumber) {
                console.log("in if verify otp");
                this.otpMatch = true;
            }
            else
                this.otpMatch = false;
        }
        else
            this.otpMatch = false;
    };
    DemoRegisterationComponent.prototype.onProfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var action;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            buttons: [
                                {
                                    text: "Choose from gallary",
                                    handler: function () {
                                        _this.chooseFile();
                                    }
                                },
                                {
                                    text: "Open Camera",
                                    handler: function () {
                                        _this.openCamera();
                                    }
                                },
                                {
                                    text: "Cancel",
                                    role: "cancel"
                                },
                            ]
                        })];
                    case 1:
                        action = _a.sent();
                        return [4 /*yield*/, action.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoRegisterationComponent.prototype.chooseFile = function () {
        var _this = this;
        var options = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.cropImage(imageData);
        });
    };
    DemoRegisterationComponent.prototype.openCamera = function () {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.cropImage(imageData);
        });
    };
    DemoRegisterationComponent.prototype.cropImage = function (imageData) {
        var _this = this;
        this.crop.crop(imageData, { quality: 100 }).then(function (cropedImage) {
            console.log("cropped image", cropedImage);
            var imagePath = cropedImage.split("?")[0];
            var copyPath = imagePath;
            var splitPath = copyPath.split("/");
            var imageName = splitPath[splitPath.length - 1];
            var filePath = imagePath.split(imageName)[0];
            console.log("image name", imageName);
            console.log("file path", filePath);
            _this.file.readAsDataURL(filePath, imageName).then(function (dataUrl) {
                console.log("data url", dataUrl);
                _this.imageURl = dataUrl;
                _this.demoInfo.imageData = dataUrl;
            });
        });
    };
    DemoRegisterationComponent = __decorate([
        core_1.Component({
            selector: "app-demo-registeration",
            templateUrl: "./demo-registeration.component.html",
            styleUrls: ["./demo-registeration.component.scss"]
        })
    ], DemoRegisterationComponent);
    return DemoRegisterationComponent;
}());
exports.DemoRegisterationComponent = DemoRegisterationComponent;
