import { UserInfoService } from "./../../services/user-info.service";
import { Storage } from "@ionic/storage";
import { UserInfo } from "./../../main/main.page";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { Component, OnInit, Input } from "@angular/core";
import { StateListService } from "src/app/services/state-list.service";
import { DistrictListService } from "./../../services/district-list.service";
import {
  ActionSheetController,
  PopoverController,
  LoadingController,
} from "@ionic/angular";
import { Chooser, ChooserResult } from "@ionic-native/chooser/ngx";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { File } from "@ionic-native/file/ngx";
import { DomSanitizer } from "@angular/platform-browser";
import { FilePath } from "@ionic-native/file-path/ngx";
import {
  FileChooser,
  FileChooserOptions,
} from "@ionic-native/file-chooser/ngx";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { LoadingServiceService } from "src/app/services/loading-service.service";
import { StorageServiceService } from "./../../services/storage-service.service";
import { InternetServiceService } from "./../../services/internet-service.service";
import { ApiServiceService } from "./../../services/api-service.service";
import {
  FileTransfer,
  FileUploadOptions,
} from "@ionic-native/file-transfer/ngx";
import { Crop } from "@ionic-native/crop/ngx";

export interface Details {
  dob: string;
  state: string;
  district: string;
  tal: string;
  parentContact: string;
  class: string;
  gender: string;
  imagePath: string;
}
const win: any = window;
@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"],
})
export class UserDetailsComponent implements OnInit {
  @Input() userInfo: UserInfo;

  stateList: {
    stateId: null;
    stateName: "";
  }[] = [];

  districtList: {
    districtId: null;
    districtName: "";
  }[] = [];

  fileObj;
  imageURl: string = "";

  constructor(
    private stateListService: StateListService,
    private databaseServiceService: DatabaseServiceService,
    private loadingServiceService: LoadingServiceService,
    private userInfoService: UserInfoService,
    private districtListService: DistrictListService,
    private actionSheetController: ActionSheetController,
    private loadingController: LoadingController,
    private popoverController: PopoverController,
    private storage: Storage,
    private camera: Camera,
    private file: File,
    private fileTransfer: FileTransfer,
    public sanitizer: DomSanitizer,
    private filePath: FilePath,
    private fileChooser: FileChooser,
    private router: Router,
    private http: HttpClient,
    private storageService: StorageServiceService,
    private internetServiceService: InternetServiceService,
    private apiService: ApiServiceService,
    private crop: Crop
  ) {
    this.stateList = [];
    this.databaseServiceService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.stateListService.getAllStates().then((result) => {
          this.stateList = result;
          // console.log(this.stateList);
        });
      }
    });
  }

  ngOnInit() {
    this.storage.ready().then((ready) => {
      if (ready) {
        // this.storage.get("userInfo").then((data) => {
        //   this.userInfo = data;
        // });
        this.storage.set("userDetailsPopover", true);
      }
    });
  }

  getDistict() {
    this.databaseServiceService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.districtListService
          .getCityList(this.userInfo.state)
          .then((result) => {
            this.districtList = result;
            // console.log(this.districtList);
          });
      }
    });
  }

  onSaveDetails() {
    // console.log("save details", this.userInfo);
    // this.databaseServiceService.getDatabaseState().subscribe((ready) => {
    //   if (ready) {
    //     this.userInfoService.insertIntoTable(this.userInfo);
    //   }
    // });
    // this.storage.ready().then((ready) => {
    //   if (ready) {
    //     this.storage.set("userLoggedIn", true).then((_) => {
    //       this.router.navigateByUrl("/dashboard/home");
    //       this.popoverController.dismiss();
    //     });
    //   }
    // });
    this.userInfo.state = this.stateList.find(
      (state) => state.stateId == this.userInfo.state
    ).stateName;
    this.userInfo.district = this.districtList.find(
      (district) => district.districtId == this.userInfo.district
    ).districtName;
    this.loadingServiceService.createLoading("Please wait...").then((_) => {
      this.databaseServiceService.getDatabaseState().subscribe((ready) => {
        if (ready) {
          this.userInfoService.insertIntoTable(this.userInfo);
          this.storage.ready().then((ready) => {
            if (ready) {
              this.storage.set("userLoggedIn", true).then((_) => {
                this.storageService.userLoggedIn = true;
                this.loadingController.dismiss().then((_) => {
                  this.router.navigateByUrl("/dashboard/home");
                  this.popoverController.dismiss();
                  this.storage.set("userDetailsPopover", false);
                });
              });
            }
          });
        }
      });
    });
    if (this.internetServiceService.networkConnected) {
      console.log("user details component", this.userInfo);
      this.http
        .post(`${this.apiService.apiUrl}fillUserDetailsUpdated`, this.userInfo)
        .subscribe(
          (data) => {
            if (data) {
              this.storage.set("dataSynced", true);
            }
          },
          (err) => this.storage.set("dataSynced", false)
        );
    } else if (this.internetServiceService.networkConnected == false)
      this.storage.set("dataSynced", false);
  }

  async onProfile() {
    const action = await this.actionSheetController.create({
      buttons: [
        {
          text: "Choose from gallary",
          handler: () => {
            this.chooseFile();
          },
        },
        {
          text: "Open Camera",
          handler: () => {
            this.openCamera();
          },
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
    });
    await action.present();
  }

  chooseFile() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      correctOrientation: true,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData);
    });
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true,
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cropImage(imageData);
    });
  }

  cropImage(imageData) {
    this.crop.crop(imageData, { quality: 100 }).then((cropedImage) => {
      console.log("cropped image", cropedImage);
      let imagePath = cropedImage.split("?")[0];
      let copyPath = imagePath;
      let splitPath = copyPath.split("/");
      let imageName = splitPath[splitPath.length - 1];
      let filePath = imagePath.split(imageName)[0];
      console.log("image name", imageName);
      console.log("file path", filePath);

      this.file.readAsDataURL(filePath, imageName).then((dataUrl) => {
        console.log("data url", dataUrl);
        this.imageURl = dataUrl;
        this.userInfo.imageData = dataUrl;
      });
    });
  }
}
