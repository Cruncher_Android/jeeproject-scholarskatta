"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.NotesChaptersPage = void 0;
var core_1 = require("@angular/core");
var NotesChaptersPage = /** @class */ (function () {
    function NotesChaptersPage(activatedRoute, chapterInfoService, fileTransfer, file, fileOpener, androidPermission, inAppBrowser, alertController, storage) {
        this.activatedRoute = activatedRoute;
        this.chapterInfoService = chapterInfoService;
        this.fileTransfer = fileTransfer;
        this.file = file;
        this.fileOpener = fileOpener;
        this.androidPermission = androidPermission;
        this.inAppBrowser = inAppBrowser;
        this.alertController = alertController;
        this.storage = storage;
        this.chapterList = [];
        this.downloading = false;
        this.loggedInType = "";
    }
    NotesChaptersPage.prototype.ngOnInit = function () {
        var _this = this;
        this.storage.get("userLoggedIn").then(function (data) {
            _this.loggedInType = data;
        });
        this.activatedRoute.paramMap.subscribe(function (subName) {
            _this.subjectId = subName.get("subjectId");
            _this.subjectName = subName.get("subjectName");
            _this.chapterInfoService.selectChapter(_this.subjectId).then(function (results) {
                results.map(function (result) {
                    return _this.chapterList.push({
                        chapter_id: result.chapter_id,
                        chapter_name: result.chapter_name,
                        subject_id: result.subject_id,
                        fileExist: false,
                        downloading: false
                    });
                });
                _this.file
                    .listDir(_this.file.externalRootDirectory + "Preparation", "JEE")
                    .then(function (fileEntry) {
                    fileEntry.map(function (entry) {
                        if (_this.chapterList.find(function (_a) {
                            var chapter_name = _a.chapter_name;
                            return chapter_name === entry.name.split(".")[0];
                        }))
                            _this.chapterList.find(function (_a) {
                                var chapter_name = _a.chapter_name;
                                return chapter_name === entry.name.split(".")[0];
                            }).fileExist = true;
                    });
                    // console.log("chapterlist in notes", this.chapterList);
                });
            });
        });
    };
    NotesChaptersPage.prototype.onDownloadFile = function (chapterName, index) {
        var _this = this;
        if (index > 1 && this.loggedInType == "demo") {
            this.createAlert("Please logout and register from login page to get complete access");
            return;
        }
        this.androidPermission
            .checkPermission(this.androidPermission.PERMISSION.WRITE_EXTERNAL_STORAGE)
            .then(function (result) {
            if (!result.hasPermission) {
                _this.androidPermission
                    .requestPermission(_this.androidPermission.PERMISSION.WRITE_EXTERNAL_STORAGE)
                    .then(function (response) {
                    _this.download(chapterName);
                });
            }
            else {
                _this.download(chapterName);
            }
        });
    };
    NotesChaptersPage.prototype.onOpenFile = function (chapterName, index) {
        if (index > 1 && this.loggedInType == "demo") {
            this.createAlert("Please logout and register from login page to get complete access");
            return;
        }
        var path = this.file.externalRootDirectory + "Preparation/JEE/" + chapterName + ".pdf";
        this.fileOpener.open(path, "application/pdf").then(function (_) {
            // console.log("In opener");
        }
        // (err) => console.log(JSON.stringify(err))
        );
    };
    NotesChaptersPage.prototype.download = function (chapterName) {
        var _this = this;
        var path = this.file.externalRootDirectory + "Preparation/JEE";
        // console.log("path", path);
        this.chapterList.find(function (_a) {
            var chapter_name = _a.chapter_name;
            return chapter_name === chapterName;
        }).downloading = true;
        var transfer = this.fileTransfer.create();
        transfer
            .download("http://www.cetonlinetest.com/ChapterNotes/PDF/NewskPDF/" + chapterName + ".pdf", path + "/" + chapterName + ".pdf")
            // transfer
            //   .download(
            //     `http://www.cetonlinetest.com/ChapterNotes/PDF/JEE/${chapterName}.pdf`,
            //     `${path}/${chapterName}.pdf`
            //   )
            .then(function (entry) {
            _this.chapterList.find(function (_a) {
                var chapter_name = _a.chapter_name;
                return chapter_name === chapterName;
            }).downloading = false;
            _this.chapterList.find(function (_a) {
                var chapter_name = _a.chapter_name;
                return chapter_name === chapterName;
            }).fileExist = true;
            var url = entry.toURL();
            // console.log("entry", entry);
            // console.log("url", url);
            _this.fileOpener.open(url, "application/pdf").then(function (_) {
                // console.log("In opener");
            }
            // (err) => console.log(JSON.stringify(err))
            );
        }
        // (err) => console.log(JSON.stringify(err))
        );
    };
    NotesChaptersPage.prototype.onChapterName = function (chapterName, fileExists, index) {
        if (index > 1 && this.loggedInType == "demo") {
            this.createAlert("Please logout and register from login page to get complete access");
            return;
        }
        if (fileExists) {
            this.onOpenFile(chapterName, index);
        }
        else if (!fileExists) {
            this.inAppBrowser.create("https://docs.google.com/viewer?url=http://www.cetonlinetest.com/ChapterNotes/PDF/NewskPDF/" + chapterName + ".pdf&embedded=true", "_blank", { location: "no" });
            // this.inAppBrowser.create(
            //   `https://docs.google.com/viewer?url=http://www.cetonlinetest.com/ChapterNotes/PDF/JEE/${chapterName}.pdf&embedded=true`,
            //   "_blank",
            //   { location: "no" }
            // );
        }
    };
    NotesChaptersPage.prototype.createAlert = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            subHeader: message,
                            cssClass: "alert-title",
                            animated: true,
                            buttons: [
                                {
                                    role: "cancel",
                                    text: "Ok"
                                },
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NotesChaptersPage = __decorate([
        core_1.Component({
            selector: "app-notes-chapters",
            templateUrl: "./notes-chapters.page.html",
            styleUrls: ["./notes-chapters.page.scss"]
        })
    ], NotesChaptersPage);
    return NotesChaptersPage;
}());
exports.NotesChaptersPage = NotesChaptersPage;
