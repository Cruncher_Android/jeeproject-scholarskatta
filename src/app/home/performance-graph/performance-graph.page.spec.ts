import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerformanceGraphPage } from './performance-graph.page';

describe('PerformanceGraphPage', () => {
  let component: PerformanceGraphPage;
  let fixture: ComponentFixture<PerformanceGraphPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerformanceGraphPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PerformanceGraphPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
