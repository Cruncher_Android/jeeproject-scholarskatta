"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.MainPage = void 0;
var forgot_password_component_1 = require("../components/forgot-password/forgot-password.component");
var core_1 = require("@angular/core");
var user_details_component_1 = require("../components/user-details/user-details.component");
var demo_registeration_component_1 = require("./../components/demo-registeration/demo-registeration.component");
var MainPage = /** @class */ (function () {
    function MainPage(device, network, dialogs, storage, router, alertController, popoverController, loadingController, http, inAppBrowser, loadingServiceService, alertServiceService, userInfoService, storageService, internetServiceService, apiService, otpService) {
        this.device = device;
        this.network = network;
        this.dialogs = dialogs;
        this.storage = storage;
        this.router = router;
        this.alertController = alertController;
        this.popoverController = popoverController;
        this.loadingController = loadingController;
        this.http = http;
        this.inAppBrowser = inAppBrowser;
        this.loadingServiceService = loadingServiceService;
        this.alertServiceService = alertServiceService;
        this.userInfoService = userInfoService;
        this.storageService = storageService;
        this.internetServiceService = internetServiceService;
        this.apiService = apiService;
        this.otpService = otpService;
        this.uuid = "";
        this.serialNo = "";
        this.randomNo = "";
        this.offLineRegistration = false;
        this.checkBox = true;
        this.deviceId = "";
        this.userInfo = {
            firstName: "",
            lastName: "",
            contactNo: "",
            email: "",
            password: "",
            birthDate: "",
            gender: "",
            parentsContactNo: "",
            standard: "",
            state: "",
            district: "",
            taluka: "",
            appId: "2",
            deviceId: "",
            distributerId: "01",
            appKey: "",
            regiDate: "",
            expireDate: "",
            status: "",
            activated: "",
            imageData: ""
        };
        this.loginInfo = {
            email: "",
            password: "",
            deviceId: "",
            distributerId: "",
            appId: ""
        };
        this.date = "";
        this.month = "";
        this.year = "";
        this.reTypePass = "";
        this.inputType = "password";
        this.iconName = "eye";
        this.passInputType = "password";
        this.passIconName = "eye";
        this.reTypePassInputType = "password";
        this.reTypePassIconName = "eye";
        this.time = "";
        this.enteredOTP = "";
        this.otpMatch = false;
        this.otpSent = false;
        this.ShowEmailIdError = false;
    }
    MainPage.prototype.ngOnInit = function () {
        // this.network.onDisconnect().subscribe((_) => {
        //   this.connected = false;
        // });
        // this.network.onConnect().subscribe((_) => {
        //   this.connected = true;
        // });
    };
    MainPage.prototype.ionViewDidEnter = function () {
        this.createDeviceId();
    };
    MainPage.prototype.createDeviceId = function () {
        this.uuid = "";
        this.serialNo = "";
        this.deviceId = "";
        this.uuid = this.device.uuid;
        this.serialNo = this.device.serial;
        // console.log(this.uuid);
        // console.log(this.serialNo);
        // this.date = new Date().getDate().toString();
        // if (this.date.length == 1) this.date = "0" + this.date;
        // this.month = (new Date().getMonth() + 1).toString();
        // if (this.month.length == 1) this.month = "0" + this.month;
        // this.year = new Date().getFullYear().toString().substr(2);
        // this.userInfo.regiDate =
        //   this.date + "-" + this.month + "-" + "20" + this.year;
        // if (this.month == "01") this.month = "A";
        // if (this.month == "02") this.month = "B";
        // if (this.month == "03") this.month = "C";
        // if (this.month == "04") this.month = "D";
        // if (this.month == "05") this.month = "E";
        // if (this.month == "06") this.month = "F";
        // if (this.month == "07") this.month = "G";
        // if (this.month == "08") this.month = "H";
        // if (this.month == "09") this.month = "I";
        // if (this.month == "10") this.month = "J";
        // if (this.month == "11") this.month = "K";
        // if (this.month == "12") this.month = "L";
        // if (this.uuid != "unknown") {
        for (var i = 0; i < this.uuid.length; i = i + 2) {
            this.deviceId = this.deviceId + this.uuid.substr(i, 1);
        }
        this.deviceId = this.deviceId.toUpperCase();
        this.userInfo.deviceId = this.deviceId;
        // let key1 = this.deviceId.substr(0, 2) + this.userInfo.distributerId;
        // let key2 =
        //   this.deviceId.substr(2, 1) +
        //   this.date.substr(0, 1) +
        //   this.deviceId.substr(3, 1) +
        //   this.date.substr(1, 1);
        // let key3 =
        //   this.deviceId.substr(4, 1) +
        //   this.year.substr(0, 1) +
        //   this.deviceId.substr(5, 1) +
        //   this.year.substr(1, 1);
        // let key4 = this.deviceId.substr(6, 2) + this.month + this.userInfo.appId;
        // this.userInfo.deviceId = key1 + "-" + key2 + "-" + key3 + "-" + key4;
        // console.log("userInfo.deviceId", this.userInfo.deviceId);
        //  }
        //else if (this.serialNo != "unknown") {
        //   this.storage.ready().then((ready) => {
        //     if (ready) {
        //       this.storage.get("serialNo").then((data) => {
        //         if (!data) {
        //           if (this.serialNo.length < 16) {
        //             let diff = 16 - this.serialNo.length;
        //             for (let i = 0; i < diff; i++) {
        //               this.serialNo =
        //                 this.serialNo + Math.floor(Math.random() * 10).toString();
        //             }
        //             for (let i = 0; i < this.serialNo.length; i = i + 2) {
        //               this.deviceId = this.deviceId + this.serialNo.substr(i, 1);
        //             }
        //             this.deviceId = this.deviceId.toUpperCase();
        //             console.log("this.deviceId", this.deviceId);
        //             this.storage.ready().then((ready) => {
        //               if (ready) {
        //                 this.storage.set("serialNo", this.deviceId);
        //               }
        //             });
        //             let key1 =
        //               this.deviceId.substr(0, 2) +
        //               this.userInfo.distributerId.substr(0, 1);
        //             let key2 =
        //               this.userInfo.distributerId.substr(1, 1) +
        //               this.deviceId.substr(2, 1) +
        //               this.date.substr(0, 1) +
        //               this.deviceId.substr(3, 1) +
        //               this.date.substr(1, 1);
        //             let key3 =
        //               this.deviceId.substr(4, 1) +
        //               this.year.substr(0, 1) +
        //               this.deviceId.substr(5, 1);
        //             let key4 =
        //               this.year.substr(1, 1) +
        //               this.deviceId.substr(6, 2) +
        //               this.month +
        //               this.userInfo.appId;
        //             this.userInfo.deviceId =
        //               key1 + "-" + key2 + "-" + key3 + "-" + key4;
        //             console.log("userInfo.deviceId", this.userInfo.deviceId);
        //           }
        //         }
        //       });
        //     }
        //   });
        // }
    };
    MainPage.prototype.onRegisterClick = function () {
        this.userInfo = {
            firstName: "",
            lastName: "",
            contactNo: "",
            email: "",
            password: "",
            birthDate: "",
            gender: "",
            parentsContactNo: "",
            standard: "",
            state: "",
            district: "",
            taluka: "",
            appId: "2",
            deviceId: "",
            distributerId: "01",
            appKey: "",
            regiDate: "",
            expireDate: "",
            status: "",
            activated: "",
            imageData: ""
        };
        this.reTypePass = "";
        this.enteredOTP = "";
        this.passwordMatch = null;
        this.otpMatch = false;
        this.otpSent = false;
        this.storageService.registeration = true;
        this.offLineRegistration = false;
    };
    MainPage.prototype.onLoginClick = function () {
        this.loginInfo = {
            email: "",
            password: "",
            deviceId: "",
            distributerId: "",
            appId: ""
        };
        this.storageService.registeration = false;
    };
    MainPage.prototype.login = function (loginForm) {
        var _this = this;
        // this.storage.set("userLoggedIn", true).then((_) => {
        //  this.storageService.userLoggedIn = true;
        // this.router.navigateByUrl("/dashboard/home");
        // })
        if (this.internetServiceService.networkConnected == true) {
            this.loginInfo.distributerId = this.userInfo.distributerId;
            this.loginInfo.appId = this.userInfo.appId;
            this.loadingServiceService.createLoading("Logging in...");
            this.http
                .post(this.apiService.apiUrl + "loginUpdated", this.loginInfo)
                .subscribe(function (data) {
                _this.checkLogin(data, loginForm);
            }, function (err) {
                _this.loginInfo.appId = "1";
                _this.http
                    .post(_this.apiService.apiUrl + "loginUpdated", _this.loginInfo)
                    .subscribe(function (data) {
                    _this.checkLogin(data, loginForm);
                }, function (err) {
                    _this.loadingController
                        .dismiss()
                        .then(function (_) {
                        return _this.alertServiceService.createAlert("Email Id or Password is incorrect.");
                    });
                });
            });
        }
        else if (this.internetServiceService.networkConnected == false) {
            this.alertServiceService.createAlert("Check your internet connection before proceeding to login");
        }
    };
    MainPage.prototype.checkLogin = function (data, loginForm) {
        var _this = this;
        this.userInfo = data;
        console.log("user info", this.userInfo);
        console.log("deviceId", this.deviceId);
        if (this.userInfo.deviceId == this.deviceId) {
            this.setLoginStatus(loginForm);
        }
        else if (this.userInfo.activated == "false") {
            this.loadingController
                .dismiss()
                .then(function (_) {
                return _this.alertServiceService.createAlert("Your account is disabled. Contact to your distributer.");
            });
        }
        else if (this.userInfo.status == "loggedIn") {
            this.loadingController
                .dismiss()
                .then(function (_) {
                return _this.alertServiceService.createAlert("Logout from other devices");
            });
        }
        else if (this.userInfo.status == "loggedOut" ||
            this.userInfo.status == null) {
            this.setLoginStatus(loginForm);
        }
    };
    MainPage.prototype.setLoginStatus = function (loginForm) {
        var _this = this;
        this.loginInfo.deviceId = this.deviceId;
        this.http
            .post(this.apiService.apiUrl + "setLoginStatusWithDeviceIdUpdated", this.loginInfo)
            .subscribe(function (_) {
            _this.getExpiryDate();
            _this.storage.ready().then(function (ready) {
                if (ready) {
                    _this.storage.set("userLoggedIn", true).then(function (_) {
                        _this.storageService.userLoggedIn = true;
                        _this.userInfoService
                            .insertIntoTable(_this.userInfo)
                            .then(function (_) {
                            _this.loadingController.dismiss().then(function (_) {
                                loginForm.resetForm();
                                _this.loginInfo = {
                                    email: "",
                                    password: "",
                                    deviceId: "",
                                    appId: "",
                                    distributerId: ""
                                };
                                _this.router.navigateByUrl("/dashboard/home");
                            });
                        });
                    });
                }
            });
        }, function (err) { });
    };
    MainPage.prototype.getExpiryDate = function () {
        var expireDate = this.userInfo.expireDate.split("-");
        this.userInfo.expireDate =
            expireDate[2].substr(2) + expireDate[1] + expireDate[0];
    };
    MainPage.prototype.offlineRegister = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.passwordMatch == true)) return [3 /*break*/, 1];
                        this.userInfo.deviceId = this.deviceId;
                        this.storage.ready().then(function (ready) {
                            if (ready) {
                                // console.log("ready");
                                _this.storage.set("userInfo", _this.userInfo).then(function () { return __awaiter(_this, void 0, void 0, function () {
                                    var alert;
                                    var _this = this;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, this.alertController.create({
                                                    animated: true,
                                                    subHeader: "Check your details before proceed for offline registration",
                                                    buttons: [
                                                        {
                                                            text: "cancel",
                                                            role: "cancel"
                                                        },
                                                        {
                                                            text: "Procced",
                                                            handler: function () {
                                                                _this.handleProceed();
                                                            }
                                                        },
                                                    ]
                                                })];
                                            case 1:
                                                alert = _a.sent();
                                                return [4 /*yield*/, alert.present()];
                                            case 2:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                            }
                        });
                        return [3 /*break*/, 4];
                    case 1: return [4 /*yield*/, this.alertController.create({
                            subHeader: "Enter correct password",
                            animated: true,
                            buttons: [
                                {
                                    text: "Ok",
                                    role: "cancel"
                                },
                            ]
                        })];
                    case 2:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.handleProceed = function () {
        var _this = this;
        // console.log("handle proceed", this.userInfo);
        if (this.offLineRegistration) {
            this.storage.set("userInfo", this.userInfo).then(function (_) {
                _this.userInfo = null;
                _this.reTypePass = "";
                _this.enteredOTP = "";
                _this.router.navigateByUrl("offline-verification");
            });
        }
    };
    MainPage.prototype.onlineRegister = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.loadingServiceService
                    .createLoading("Please wait processing your request...")
                    .then(function (_) {
                    var date = new Date();
                    var minutes = date.getMinutes().toString();
                    var hours = date.getHours();
                    if (hours < 12)
                        _this.time = "AM";
                    else if (hours > 12) {
                        hours = hours - 12;
                        _this.time = "PM";
                    }
                    else if (hours == 12)
                        _this.time = "PM";
                    if (minutes.length == 1)
                        minutes = "0" + minutes;
                    var notiDate = date.getDate().toString() +
                        "-" +
                        (date.getMonth() + 1).toString() +
                        "-" +
                        date.getFullYear().toString();
                    var notiTime = hours.toString() + ":" + minutes + " " + _this.time;
                    // console.log("onLine register", this.userInfo);
                    if (_this.internetServiceService.networkConnected) {
                        _this.userInfo.deviceId = _this.deviceId;
                        _this.http
                            .post(_this.apiService.apiUrl + "checkRegisterWithDeviceIdAndAppID", {
                            email: _this.userInfo.email,
                            distributerId: _this.userInfo.distributerId,
                            appId: _this.userInfo.appId
                        })
                            .subscribe(function (data) {
                            if (data) {
                                _this.http
                                    .post(_this.apiService.apiUrl + "notify", __assign(__assign({}, _this.userInfo), { notiDate: notiDate,
                                    notiTime: notiTime }))
                                    .subscribe(function (data) {
                                    if (data) {
                                        _this.stopInterval = setInterval(function () {
                                            _this.checkVerificationKey();
                                        }, 2000);
                                    }
                                });
                            }
                            else if (!data) {
                                _this.loadingController
                                    .dismiss()
                                    .then(function (_) {
                                    return _this.alertServiceService.createAlert("Email already registered");
                                });
                            }
                        });
                    }
                    else if (_this.internetServiceService.networkConnected == false) {
                        _this.loadingController
                            .dismiss()
                            .then(function (_) {
                            return _this.alertServiceService.createAlert("Check your internet connection before proceeding");
                        });
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MainPage.prototype.payAndRegister = function () {
        var _this = this;
        var options = {
            location: "yes",
            clearcache: "yes",
            zoom: "yes",
            toolbar: "no",
            closebuttoncaption: "Close"
        };
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var fullYear = new Date().getFullYear() + 1;
        this.userInfo.deviceId = this.deviceId;
        this.loadingServiceService
            .createLoading("Please wait processing your request...")
            .then(function (_) {
            _this.http
                .post(_this.apiService.apiUrl + "checkRegisterWithDeviceIdAndAppID", {
                email: _this.userInfo.email,
                distributerId: _this.userInfo.distributerId,
                appId: _this.userInfo.appId
            })
                .subscribe(function (data) {
                if (data) {
                    _this.http
                        .post(_this.apiService.apiUrl + "getPaymentLink", { appId: _this.userInfo.appId }, {})
                        .subscribe(function (data) {
                        var url = data[0];
                        _this.loadingController.dismiss().then(function (_) {
                            var browser = _this.inAppBrowser.create(url, "_self", options);
                            browser.on("loadstart").subscribe(function (event) {
                                console.log("event", event);
                                if (event.url.startsWith("https://payu.in/paySuccess")) {
                                    setTimeout(function () {
                                        browser.close();
                                        var expityDate = date.toString() + "-" + month.toString() + "-" + fullYear.toString();
                                        _this.showPaymentReciept(expityDate);
                                    }, 2000);
                                }
                                else if (event.url.startsWith("https://payu.in/payFailure")) {
                                    setTimeout(function () {
                                        browser.close();
                                    }, 2000);
                                }
                            });
                            // this.handlePaymentSuccess();
                        });
                    });
                }
                else if (!data) {
                    _this.loadingController
                        .dismiss()
                        .then(function (_) {
                        return _this.alertServiceService.createAlert("Email already registered");
                    });
                }
            });
        });
        // this.showUserDetailsComponent();
    };
    MainPage.prototype.handlePaymentSuccess = function () {
        var _this = this;
        this.loadingServiceService
            .createLoading("Saving your Details...")
            .then(function (_) {
            var date = new Date().getDate();
            var month = new Date().getMonth() + 1;
            var fullYear = new Date().getFullYear();
            var year = parseInt(new Date().getFullYear().toString().substr(2));
            _this.timeStamp = {
                deviceId: _this.userInfo.deviceId,
                appId: _this.userInfo.appId,
                distributerId: _this.userInfo.distributerId,
                daysCount: 0,
                monthsCount: 0,
                yearsCount: 1,
                regiDate: date,
                regiMonth: month,
                regiYear: year,
                date: ""
            };
            _this.http
                .post(_this.apiService.apiUrl + "userRegister", _this.timeStamp)
                .subscribe(function (data) {
                _this.userInfo.appKey = data[0];
                _this.userInfo.expireDate = data[1];
                _this.userInfo.regiDate =
                    date.toString() +
                        "-" +
                        month.toString() +
                        "-" +
                        fullYear.toString();
                _this.userInfo.status = "loggedIn";
                _this.userInfo.activated = "true";
                _this.http
                    .post(_this.apiService.apiUrl + "setUserInfo", __assign(__assign({}, _this.userInfo), { activationGivenBy: "Sancheti Classes" }))
                    .subscribe(function (data) {
                    console.log(data);
                    _this.getExpiryDate();
                    _this.loadingController.dismiss().then(function (_) {
                        _this.showUserDetailsComponent();
                        _this.reTypePass = "";
                    });
                });
            });
        });
    };
    MainPage.prototype.checkVerificationKey = function () {
        var _this = this;
        console.log("check verification key", this.userInfo);
        this.http
            .post(this.apiService.apiUrl + "getUserInfoUpdated", this.userInfo)
            .subscribe(function (data) {
            if (data) {
                clearTimeout(_this.stopInterval);
                _this.userInfo = data;
                _this.getExpiryDate();
                _this.loadingController.dismiss().then(function (_) {
                    _this.showUserDetailsComponent();
                });
                _this.reTypePass = "";
            }
        }
        // (err) => console.log(err)
        );
    };
    MainPage.prototype.showUserDetailsComponent = function () {
        return __awaiter(this, void 0, void 0, function () {
            var popover;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverController.create({
                            component: user_details_component_1.UserDetailsComponent,
                            componentProps: {
                                userInfo: this.userInfo
                            },
                            cssClass: "myPopOver",
                            animated: true,
                            backdropDismiss: false
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.onCheckBoxClick = function () {
        var _this = this;
        this.offLineRegistration = !this.offLineRegistration;
        // console.log("offLineStorage", this.offLineRegistration);
        if (this.offLineRegistration == true) {
            if (this.uuid == "unknown" && this.serialNo == "unknown") {
                this.storage.ready().then(function (ready) {
                    if (ready) {
                        // console.log("in ready");
                        _this.storage.get("randomNo").then(function (data) {
                            if (!data) {
                                _this.randomNo = "";
                                for (var i = 0; i < 8; i++) {
                                    _this.randomNo =
                                        _this.randomNo +
                                            _this.userInfo.contactNo.charAt(Math.floor(Math.random() * 10));
                                }
                                // console.log("randomNo", this.randomNo);
                                _this.deviceId = _this.randomNo;
                                _this.storage.ready().then(function (ready) {
                                    if (ready) {
                                        _this.storage.set("randomNo", _this.deviceId);
                                    }
                                });
                                var key1 = _this.deviceId.substr(0, 2) +
                                    _this.userInfo.distributerId +
                                    _this.deviceId.substr(2, 1);
                                var key2 = _this.date.substr(0, 1) +
                                    _this.deviceId.substr(3, 1) +
                                    +_this.date.substr(1, 1);
                                var key3 = _this.deviceId.substr(4, 1) +
                                    _this.year.substr(0, 1) +
                                    _this.deviceId.substr(5, 1) +
                                    _this.year.substr(1, 1) +
                                    _this.deviceId.substr(6, 1);
                                var key4 = _this.deviceId.substr(7, 1) + _this.month + _this.userInfo.appId;
                                _this.userInfo.deviceId =
                                    key1 + "-" + key2 + "-" + key3 + "-" + key4;
                                // console.log("userInfo.deviceId", this.userInfo.deviceId);
                            }
                        });
                    }
                });
            }
        }
    };
    MainPage.prototype.onKeyUp = function (event) {
        this.reTypePass = event.target.value;
        if (this.userInfo.password == this.reTypePass) {
            this.passwordMatch = true;
        }
        else {
            this.passwordMatch = false;
        }
    };
    MainPage.prototype.onPassword = function () {
        if (this.reTypePass.length > 0) {
            this.reTypePass = "";
            this.passwordMatch = false;
        }
    };
    MainPage.prototype.onForgotPass = function () {
        return __awaiter(this, void 0, void 0, function () {
            var popover;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.loginInfo.email == "")) return [3 /*break*/, 1];
                        this.ShowEmailIdError = true;
                        return [3 /*break*/, 4];
                    case 1:
                        this.ShowEmailIdError = false;
                        return [4 /*yield*/, this.popoverController.create({
                                component: forgot_password_component_1.ForgotPasswordComponent,
                                animated: true,
                                cssClass: "myPopOver",
                                componentProps: {
                                    emailId: this.loginInfo.email,
                                    distributerId: this.userInfo.distributerId,
                                    appId: this.userInfo.appId
                                }
                            })];
                    case 2:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.changeInputType = function () {
        if (this.inputType == "password") {
            this.inputType = "text";
            this.iconName = "eye-off";
        }
        else {
            this.inputType = "password";
            this.iconName = "eye";
        }
    };
    MainPage.prototype.changePassInputType = function () {
        if (this.passInputType == "password") {
            this.passInputType = "text";
            this.passIconName = "eye-off";
        }
        else {
            this.passInputType = "password";
            this.passIconName = "eye";
        }
    };
    MainPage.prototype.changeReTypePassInputType = function () {
        if (this.reTypePassInputType == "password") {
            this.reTypePassInputType = "text";
            this.reTypePassIconName = "eye-off";
        }
        else {
            this.reTypePassInputType = "password";
            this.reTypePassIconName = "eye";
        }
    };
    MainPage.prototype.sendOtp = function () {
        this.otpSent = true;
        this.otpMatch = false;
        this.enteredOTP = "";
        this.randomNumber = Math.floor(1000 + Math.random() * 9000);
        // this.randomNumber = 1234;
        console.log(this.randomNumber);
        // console.log(this.number);
        // console.log(this.storageService.userInfo.StudentContact);
        this.otpService.sendOtp(this.userInfo.contactNo, this.randomNumber);
    };
    MainPage.prototype.verifyOtp = function (event) {
        var otp = event.target.value;
        console.log(this.randomNumber, otp);
        if (otp.length == 4) {
            if (Number.parseInt(otp) == this.randomNumber) {
                console.log("in if verify otp");
                this.otpMatch = true;
            }
            else
                this.otpMatch = false;
        }
        else
            this.otpMatch = false;
    };
    MainPage.prototype.onDemoLoginClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var popover;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.internetServiceService.networkConnected) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.popoverController.create({
                                component: demo_registeration_component_1.DemoRegisterationComponent,
                                cssClass: "myPopOver",
                                animated: true
                            })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        if (!this.internetServiceService.networkConnected)
                            this.alertServiceService.createAlert("Check your internet connection before proceeding");
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.showPaymentReciept = function (expireDate) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            animated: true,
                            header: "Payment Successful",
                            cssClass: "alert-title",
                            subHeader: "Payment Successful, Application is valid upto " + expireDate,
                            buttons: [
                                {
                                    text: "Ok",
                                    role: "cancel"
                                },
                            ],
                            backdropDismiss: false
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        alert
                            .onDidDismiss()
                            .then(function (_) { return setTimeout(function () { return _this.handlePaymentSuccess(); }, 1000); });
                        return [2 /*return*/];
                }
            });
        });
    };
    MainPage = __decorate([
        core_1.Component({
            selector: "app-main",
            templateUrl: "./main.page.html",
            styleUrls: ["./main.page.scss"]
        })
    ], MainPage);
    return MainPage;
}());
exports.MainPage = MainPage;
