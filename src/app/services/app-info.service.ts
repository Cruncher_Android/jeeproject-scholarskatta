import { DatabaseServiceService } from './database-service.service';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AppInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }


  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if(ready) {
        this.databaseServiceService.getDataBase().executeSql('CREATE TABLE IF NOT EXISTS app_info(appid varchar(255) primary key,inst_date varchar(255),exp_date varchar(255))', []);
      }
    });
  }
}
