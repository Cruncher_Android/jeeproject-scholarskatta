"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.OtpServiceService = void 0;
var core_1 = require("@angular/core");
var OtpServiceService = /** @class */ (function () {
    function OtpServiceService(nativeHttp, toastService, alertService) {
        this.nativeHttp = nativeHttp;
        this.toastService = toastService;
        this.alertService = alertService;
    }
    OtpServiceService.prototype.sendOtp = function (number, randomNumber) {
        var _this = this;
        this.nativeHttp.setDataSerializer("json");
        return this.nativeHttp
            .get("http://173.45.76.227/send.aspx?username=Cstpvt&pass=Cstpvt@1&route=trans1&senderid=cstpvt&numbers=" + Number.parseInt(number) + "\n          &message=Your OTP for scholarskatta application registration is " + randomNumber, {}, {})
            .then(function (data) {
            var returnedValue = data.data.split("|")[0];
            console.log(returnedValue);
            if (returnedValue == "1")
                _this.toastService.createToast("OTP sent successfully.");
            else {
                _this.toastService
                    .createToast("Unable to send OTP. (" + returnedValue + ")")
                    .then(function (_) {
                    return _this.alertService.createAlert("Your OTP for scholarskatta application registration is " + randomNumber);
                });
                if (returnedValue == "2")
                    console.log("Invalid destination.");
                else if (returnedValue == "3")
                    console.log("Insufficient credit.");
                else if (returnedValue == "4")
                    console.log("Message can't sent, internet is not connected.");
                else if (returnedValue == "5")
                    console.log("Sorry failed to send message, try again with valid sender id.");
                else if (returnedValue == "6")
                    console.log("Invalid URL error, i.e. one of the parameters was not seted.");
                else if (returnedValue == "7")
                    console.log("Submission Error");
            }
        }, function (err) {
            console.log("error", err);
            _this.toastService
                .createToast("Unable to send OTP.")
                .then(function (_) {
                return _this.alertService.createAlert("Your OTP for scholarskatta application registration is " + randomNumber);
            });
        });
    };
    OtpServiceService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], OtpServiceService);
    return OtpServiceService;
}());
exports.OtpServiceService = OtpServiceService;
