import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class DistrictListService {

  constructor(private databaseServiceService: DatabaseServiceService) {}

  getCityList(stateId) {
    return this.databaseServiceService.getDataBase().executeSql('SELECT * FROM CITY_INFO WHERE STATE_ID = ?', [stateId]).then(result => {
      // console.log(result)
      const districtList = [] 
      if(result.rows.length > 0) {
        for(let i = 0; i < result.rows.length; i++) {
          districtList.push({
            districtId: result.rows.item(i).CITY_ID,
            districtName: result.rows.item(i).CITY_NAME
          })
        }
      }
      return districtList;
    })
  }

}
