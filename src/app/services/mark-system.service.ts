import { DatabaseServiceService } from './database-service.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MarkSystemService {

  constructor(private databaseServiceService: DatabaseServiceService) { }
  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS mark_system(mid integer primary key,
          pcorr_mark integer,p_incorr_mark integer,ccorr_mark integer,c_incorr_mark integer,mcorr_mark integer,m_incorr_mark integer,
          bcorr_mark integer,b_incorr_mark integer)`, []);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO mark_system
        (mid,pcorr_mark,p_incorr_mark,ccorr_mark,c_incorr_mark,mcorr_mark,m_incorr_mark)values(?,?,?,?,?,?,?)`,
        [1,4,1,4,1,4,1]);
      }
    });
  }
}
