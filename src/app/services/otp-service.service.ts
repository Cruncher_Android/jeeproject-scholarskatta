import { Injectable } from "@angular/core";
import { ToastServiceService } from "./toast-service.service";
import { HTTP } from "@ionic-native/http/ngx";
import { AlertServiceService } from "src/app/services/alert-service.service";

@Injectable({
  providedIn: "root",
})
export class OtpServiceService {
  constructor(
    private nativeHttp: HTTP,
    private toastService: ToastServiceService,
    private alertService: AlertServiceService
  ) {}

  sendOtp(number, randomNumber) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp
      .get(
        `http://173.45.76.227/send.aspx?username=Cstpvt&pass=Cstpvt@1&route=trans1&senderid=cstpvt&numbers=${Number.parseInt(
          number
        )}
          &message=Your OTP for scholarskatta application registration is ${randomNumber}`,
        {},
        {}
      )
      .then(
        (data) => {
          let returnedValue = data.data.split("|")[0];
          console.log(returnedValue);
          if (returnedValue == "1")
            this.toastService.createToast("OTP sent successfully.");
          else {
            this.toastService
              .createToast(`Unable to send OTP. (${returnedValue})`)
              .then((_) =>
                this.alertService.createAlert(
                  `Your OTP for scholarskatta application registration is ${randomNumber}`
                )
              );
            if (returnedValue == "2") console.log("Invalid destination.");
            else if (returnedValue == "3") console.log("Insufficient credit.");
            else if (returnedValue == "4")
              console.log("Message can't sent, internet is not connected.");
            else if (returnedValue == "5")
              console.log(
                "Sorry failed to send message, try again with valid sender id."
              );
            else if (returnedValue == "6")
              console.log(
                "Invalid URL error, i.e. one of the parameters was not seted."
              );
            else if (returnedValue == "7") console.log("Submission Error");
          }
        },
        (err) => {
          console.log("error", err);
          this.toastService
            .createToast("Unable to send OTP.")
            .then((_) =>
              this.alertService.createAlert(
                `Your OTP for scholarskatta application registration is ${randomNumber}`
              )
            );
        }
      );
  }
}
