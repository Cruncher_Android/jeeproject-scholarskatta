import { TestBed } from '@angular/core/testing';

import { QuestionPaperPcmMarksService } from './question-paper-pcm-marks.service';

describe('QuestionPaperPcmMarksService', () => {
  let service: QuestionPaperPcmMarksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionPaperPcmMarksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
